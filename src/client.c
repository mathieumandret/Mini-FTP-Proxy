#include <sys/socket.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netdb.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdbool.h>
#include "client.h"
#include "types.h"


int connect_to_serv(const char *hostname, const char *port) {
    int sockfd;
    int err;
    addrinfo *res;
    addrinfo hints;
    bool isConnected = false;
    bzero(&hints, sizeof(hints));
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_family = AF_UNSPEC;
    err = getaddrinfo(hostname, port, &hints, &res);
    if (err < 0) {
        fprintf(stderr, "connect_to_serv getaddrinfo: %s\n", gai_strerror(err));
        exit(1);
    }

    while (!isConnected && res != NULL) {
        sockfd = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
        if (sockfd == -1) {
            perror("Socket serveur");
            exit(2);
        }
        err = connect(sockfd, res->ai_addr, res->ai_addrlen);
        if (err == -1) {
            res = res->ai_next;
            close(sockfd);
        } else {
            isConnected = true;
        }
    }

    freeaddrinfo(res);
    if (!isConnected) {
        perror("Connexion impossible");
        exit(2);
    }
    return sockfd;
}

int getPort(const char *message) {
    char *n5;
    char *n6;
    int port;
    char delim[2] = ",";
    n5 = strtok((char *) message, delim);
    for (int i = 0; i < 4; i++) {
        n5 = strtok(NULL, delim);
    }
    n6 = strtok(NULL, delim);
    port = atoi(n5) * 256 + atoi(n6);
    return port;
}
