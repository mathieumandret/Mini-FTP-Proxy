#ifndef SERVER_H
#define SERVER_H


int init_serv(const char *p_address, const char *p_port);
void server_loop(int server_sock);
void handle_connexion(int client_sockfd);
void forward_data(int inc_sockfd, int em_sockfd);

#endif
