#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define LEN 256

void mod(char **str)
{
    printf("Valeur du paramètre: %s\n", *str);
    strcpy(*str, "kek");
    bzero(*str, sizeof(str));
    
}

int main(void)
{
    char *buffer = malloc(LEN);
    strcpy(buffer, "COMMANDE");
    printf("Valeur du buffer: %s\n", buffer);
    mod(&buffer);
    printf("Valeur du buffer: %s\n", buffer);
}
