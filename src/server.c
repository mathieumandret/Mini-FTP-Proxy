#include <netdb.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <arpa/inet.h>
#include "server.h"
#include "types.h"
#include "client.h"


#define READ_ERROR perror("read"); exit(1);
#define WRITE_ERROR perror("write"); exit(1);

#define HOSTLEN 100
#define LOGINLEN 50
#define PORTLEN 6
#define BUFFSIZE 1024
#define DATASIZE 5000
#define LISTENLEN 5

int init_serv(const char *p_address, const char *p_port) {
    int err; // Code erreur
    int option = 1;
    int sockfd; // Descripteur de la socket du serveur
    addrinfo hints; // Configuration du serveur
    addrinfo *res; // Infos sur l'adresse du serveur initialisé

    // Initaliser les propriétés
    bzero(&hints, sizeof(hints));
    hints.ai_flags = AI_PASSIVE;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_family = AF_INET; // Accepter ipv4 et ipv6
    // Recupere les infos correspondant à la configuration
    err = getaddrinfo(p_address, p_port, &hints, &res);
    if (err) {
        fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(err));
        exit(-1);
    }
    // Initialiser la socket
    sockfd = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
    if (sockfd == -1) {
        perror("Socket");
        exit(-2);
    }
    // Rendre la socket directemment reutilisable après sa fermeture
    setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &option, sizeof(option));
    // Lier les infos à la socket
    err = bind(sockfd, res->ai_addr, res->ai_addrlen);
    if (err == -1) {
        perror("Bind");
        exit(-3);
    }
    // Plus besoin de conserver les infos
    free(res);
    // Retourner la socket liée
    return sockfd;
}


void forward_serv_to_client(int inc_sockfd, int em_sockfd) {
    char buffer[BUFFSIZE];
    int err;
    // Lire depuis la socket entrante
    err = read(inc_sockfd, buffer, BUFFSIZE);
    if (err < 0) {
        READ_ERROR
    }
    // Terminer le string
    buffer[err] = '\0';
    // Ecrire dans la sortante
    err = write(em_sockfd, buffer, strlen(buffer));
    if (err < 0) {
        WRITE_ERROR
    }
}

void passive_mode_port(char *command, int server_sockfd, int client_sockfd)
{
    int err;
    int client_ip[4];
    int server_ip[4];
    struct sockaddr_in client_addr;
    struct sockaddr_in server_addr;
    socklen_t len;
    char buffer[BUFFSIZE];
    char data[DATASIZE];
    char port_client[PORTLEN];
    char port_serveur[PORTLEN];
    int server_data_sockfd;
    int client_data_sockfd;

    printf("\n\n\nCOMMANDE PORT RECUE, PASSAGE EN MODE PASSIF COTE SERVEUR\n----------------------------------------\n\n");
    printf("LE CLIENT A ENVOYE %s", command);
    // Recuperer le port du client
    sprintf(port_client, "%d", getPort(command));
    // Recuperer l'adresse IP du client
    len = sizeof(client_addr);
    getsockname(client_sockfd, (struct sockaddr*) &client_addr, &len);
    char *client_host = inet_ntoa(client_addr.sin_addr);
    sscanf(client_host, "%d.%d.%d.%d", &client_ip[0], &client_ip[1], &client_ip[2], &client_ip[3]);
    printf("Port client = %s\n", port_client);
    printf("Adresse client = %s\n", client_host);
    puts("ENVOI DE LA COMMANDE PASV AU SERVEUR");
    // Envoyer PASV au serveur
    err = write(server_sockfd, "PASV\n", 5);
    if (err < 0) {
        WRITE_ERROR
    }
    // Ecouter sa réponse
    err = read(server_sockfd, buffer, BUFFSIZE);
    if (err < 0) {
        READ_ERROR
    }
    buffer[err] = '\0';
    // Recuperer le port et l'adresse du serveur
    printf("LE SERVEUR A REPONDU: %s", buffer);
    sprintf(port_serveur, "%d", getPort(buffer));
    getsockname(server_sockfd, (struct sockaddr*) &server_addr, &len);
    char *server_host = inet_ntoa(server_addr.sin_addr);
    sscanf(server_host, "%d.%d.%d.%d", &server_ip[0], &client_ip[1], &client_ip[2], &client_ip[3]);

    printf("Port serveur = %s\n", port_serveur);
    printf("Adresse serveur %s\n", server_host);
    puts("OUVERTURE DES CANAUX DE DONNES COTE CLIENT ET SERVEUR");

    // Ouvrir le canal de données du serveur
    server_data_sockfd = connect_to_serv(server_host, port_serveur);
    // Ouvrir le canal de données du client
    client_data_sockfd = connect_to_serv(client_host, port_client);
    // Envoyer LIST sur le canal de controle du serveur
    puts("ENVOI DE LIST SUR LE CANAL DE CONTROLE DU SERVEUR");
    err = write(server_sockfd, "LIST\n", 5);
    if (err < 0) {
	WRITE_ERROR
    }
    puts("ENVOI DE 150 AU CLIENT");
    // Transmettre la réponse 150 au client
    forward_serv_to_client(server_sockfd, client_sockfd);
    // Lire les données
    err = read(server_data_sockfd, data, DATASIZE);
    if (err < 0) {
	READ_ERROR
    }
    // Terminer le string
    data[err] = '\0';
    // Données reçues
    printf("%s", data);
    // Envoyer les données au client
    write(client_data_sockfd, data, DATASIZE);
    // Envoyer le code 226 au client
    write(client_sockfd, "226\n", 4);
    
}

void forward_client_to_serv(int inc_sockfd, int em_sockfd) {
    char buffer[BUFFSIZE];
    int err;
    // Lire depuis la socket entrante
    err = read(inc_sockfd, buffer, BUFFSIZE);
    if (err < 0) {
        READ_ERROR
    }
    // Terminer le string
    buffer[err] = '\0';

    // Verifier si la commande est une commande PORT
    if (strncmp(buffer, "PORT", 4) == 0) {
	passive_mode_port(buffer, em_sockfd, inc_sockfd);
	return;
    } else {
	// Ecrire dans la sortante
	err = write(em_sockfd, buffer, strlen(buffer));
	if (err < 0) {
	    WRITE_ERROR
	}
    }
}

void server_loop(int server_sock) {
    pid_t pid;
    sockaddr_storage client_addr; // Adresse du client
    int client_sockfd; // Socket vers le client
    socklen_t len; // Taille d'une structure adresse
    // Ecouter les connexions
    listen(server_sock, LISTENLEN);
    // Gerer les connexions entrantes
    len = sizeof(sockaddr_storage);
    // Rester en attente de connexion
    while (1) {
        client_sockfd = accept(server_sock, (sockaddr *) &client_addr, &len);
        if (client_sockfd < 0) {
            perror("Socket client");
            exit(-4);
        }
        // Creer un nouveau processus pour chaque connexion
        puts("Connexion d'un client");
        pid = fork();
        if (pid < 0) {
            perror("fork");
            exit(-5);
        }
        if (pid == 0) {
            // Plus besoin de la socket d'écoute du serveur
            close(server_sock);
            // Gerer la communication
            handle_connexion(client_sockfd);
            exit(0);
        } else {
            // On est dans le père
            // fermer la socket client
            close(client_sockfd);
        }
    }
}

// Gere la connexion et l'authentification avec le serveur
void handle_connexion(int client_sockfd) {
    int err;
    int server_sockfd;
    char *tmp_login;
    char *tmp_hostname;
    char buffer[BUFFSIZE];
    char login[LOGINLEN];
    char hostname[HOSTLEN];
    // Envoyer le message d'accueil eu client
    strcpy(buffer, "220 Vous êtes connectés au proxy. Authentifiez vous avec user@serveur\n");
    err = write(client_sockfd, buffer, strlen(buffer));
    if (err < 0) {
        WRITE_ERROR
    }
    // Recuperer user@serveur
    memset(buffer, 0, BUFFSIZE);
    err = read(client_sockfd, buffer, BUFFSIZE);
    if (err < 0) {
        READ_ERROR
    }
    buffer[err] = '\0';
    printf("Reçu: %s", buffer);
    // Parser login et serveur
    tmp_login = strtok(buffer, "@");
    tmp_hostname = strtok(NULL, "@");
    tmp_login[LOGINLEN - 1] = '\0';
    hostname[HOSTLEN - 1] = '\0';

    // Copie du login et serveur POURQUOI ?
    strcpy(login, tmp_login);
    strcpy(hostname, tmp_hostname);

    // Se connecter au serveur demandé par le client
    printf("Connexion au serveur sur %s", hostname);
    server_sockfd = connect_to_serv(hostname, "21");
    // Lire la réponse du serveur
    err = read(server_sockfd, buffer, BUFFSIZE);
    if (err < 0) {
        READ_ERROR
    }
    buffer[err] = '\0';
    printf("Recu:  %s", buffer);
    printf("Envoi de %s au serveur\n", login);
    strcat(login, "\n");
    err = write(server_sockfd, login, strlen(login));
    if (err < 0) {
        WRITE_ERROR
    }

    // Demande de mot de passe
    forward_serv_to_client(server_sockfd, client_sockfd);
    // Mot de passe
    forward_client_to_serv(client_sockfd, server_sockfd);
    // 230 login successful
    forward_serv_to_client(server_sockfd, client_sockfd);
    // SYST
    forward_client_to_serv(client_sockfd, server_sockfd);
    // Réponse SYST
    forward_serv_to_client(server_sockfd, client_sockfd);
    // PORT
    forward_client_to_serv(client_sockfd, server_sockfd);
}
